module GamesHelper
	def get_dealer_class(game)
		game.winner == "dealer" ? "won" : "lost"
	end

	def get_user_class(game)
		game.winner == "user" ? "won" : "lost"
	end
end
