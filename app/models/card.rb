class Card < ActiveRecord::Base
  belongs_to :step

  RANGE = (2..10)
  NUMBERS = RANGE.to_a.map { |i| i.to_s }
  ROYALS = ["J", "Q", "K"]
  RANKS = ["A"] + NUMBERS + ROYALS

  def Card.get_card_rank
  	RANKS[rand(12)]
  end

  def Card.get_rank_val_mapping
  	mappings = {}
  	RANGE.each{|r| mappings["#{r}"] = r}
  	ROYALS.each{|r| mappings[r] = 10}
  	mappings["A"] = 11
  	mappings
  end
end
