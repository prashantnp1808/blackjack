class Game < ActiveRecord::Base
  has_many :steps, dependent: :destroy
  has_many :cards, through: :steps

  def initialize_steps
  	step = self.steps.create(step_count: 1)
  	step.initialize_cards
  end

  def update_status
  	user_score, dealer_score = get_score
	if (user_score == 21) || (dealer_score > 21) || ((user_score < 21) && (user_score > dealer_score) && (dealer_score >= 17))
		winner = GlobalConstants::USER
		status = GlobalConstants::COMPLETE
	elsif (dealer_score == 21) || (user_score > 21) || ((user_score < 21) && (dealer_score > user_score) && (dealer_score >= 17))
		winner = GlobalConstants::DEALER
		status = GlobalConstants::COMPLETE
	else
		winner = nil
		status = GlobalConstants::PROGRESS
	end
	self.update_attributes(winner: winner, user_score: user_score, dealer_score: dealer_score, status: status)
  end

  def get_score
  	user_score = get_score_of(GlobalConstants::USER)
  	dealer_score = get_score_of(GlobalConstants::DEALER)
	[user_score, dealer_score]
  end

  def completed?
  	self.status == GlobalConstants::COMPLETE
  end

  def get_cards
  	dealer_cards = self.cards.where(card_for: GlobalConstants::DEALER)
  	user_cards = self.cards.where(card_for: GlobalConstants::USER)
  	[dealer_cards, user_cards]
  end

  def next_step
  	self.steps.create(step_count: self.steps.last.step_count + 1)
  end

  def get_score_of(card_for)
  	cards = self.cards.where(card_for: card_for)
  	score = 0
  	cards.each do |card|
		score += Card.get_rank_val_mapping[card.rank]
	end
	score
  end
end
