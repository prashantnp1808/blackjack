class Step < ActiveRecord::Base
  belongs_to :game
  has_many :cards, dependent: :destroy

  def initialize_cards
  	self.cards.create(card_for: GlobalConstants::DEALER, rank: Card.get_card_rank)
  	self.cards.create(card_for: GlobalConstants::USER, rank: Card.get_card_rank)
  	self.cards.create(card_for: GlobalConstants::USER, rank: Card.get_card_rank)
  end

  def pick_a_card
  	self.cards.create(card_for: GlobalConstants::USER, rank: Card.get_card_rank)
  end

  def skip
  	self.cards.create(card_for: GlobalConstants::DEALER, rank: Card.get_card_rank)
  end
end
