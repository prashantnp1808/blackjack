class GamesController < ApplicationController
	def welcome
	end

	def start
		@game = Game.create(status: GlobalConstants::PROGRESS)
		@game.initialize_steps
		@game.update_status
		@dealer_cards, @user_cards = @game.get_cards
		if @game.completed?
			redirect_to :action => 'results', :id => @game.id and return
		end
	end

	def results
		@game = Game.find(params[:id])
	end

	def pick_a_card
		@game = Game.find(params[:id])
		@step = @game.next_step
		@step.pick_a_card
		@game.update_status
		@dealer_cards, @user_cards = @game.get_cards
		if @game.completed?
			redirect_to :action => 'results', :id => @game.id and return
		end
		render 'start'
	end

	def skip
		@game = Game.find(params[:id])
		@step = @game.next_step
		while @game.get_score_of(GlobalConstants::DEALER) <= 16
			@step.skip
		end
		@game.update_status
		@dealer_cards, @user_cards = @game.get_cards
		if @game.completed?
			redirect_to :action => 'results', :id => @game.id and return
		end
		render 'start'
	end

	def statistics
		@games = Game.all
	end
end
