Blackjack::Application.routes.draw do
  resources :games, except: [:index, :new, :create, :show, :edit, :update, :destroy] do
    member do
      get 'pick_a_card'
      get 'skip'
      get 'results'
    end
    collection do
      get 'start'
      get 'statistics'
    end
  end
  root 'games#welcome'
end
