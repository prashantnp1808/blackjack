class CreateSteps < ActiveRecord::Migration
  def change
    create_table :steps do |t|
      t.integer :step_count
      t.references :game, index: true

      t.timestamps
    end
  end
end
