class CreateCards < ActiveRecord::Migration
  def change
    create_table :cards do |t|
      t.string :card_for
      t.string :rank
      t.references :step, index: true

      t.timestamps
    end
  end
end
