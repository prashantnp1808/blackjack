class CreateGames < ActiveRecord::Migration
  def change
    create_table :games do |t|
      t.integer :won_by
      t.string :winner
      t.string :status
      t.integer :user_score
      t.integer :dealer_score
      t.integer :bet

      t.timestamps
    end
  end
end
